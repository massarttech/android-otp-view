# OTP TextView [![](https://jitpack.io/v/com.gitlab.massarttech/android-otp-view.svg)](https://jitpack.io/#com.gitlab.massarttech/android-otp-view)
**An OTP Box implementation for case when a single digit should be entered Individually..**

<img src="https://gitlab.com/massarttech/android-otp-view/raw/master/screenshot.png?inline=false" width="300" height="500"/>

### Setup
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:
```
dependencies {
    implementation 'com.gitlab.massarttech:android-otp-view:1.0.0'
}
```

### Usage
```xml
 <com.massarttech.android.otptextview.OtpTextView
         android:id="@+id/otp_view"
         android:layout_width="wrap_content"
         android:layout_height="wrap_content"
         android:textColor="#ffffff"
         app:bar_active_color="@color/white"
         app:bar_enabled="true"
         app:bar_error_color="@color/red"
         app:bar_height="1.5dp"
         app:bar_inactive_color="@color/whiteoff"
         app:bar_margin_bottom="0dp"
         app:bar_margin_left="2dp"
         app:bar_margin_right="2dp"
         app:bar_success_color="@color/green"
         app:box_margin="0dp"
         app:height="40dp"
         app:hide_otp="true"
         app:layout_constraintBottom_toBottomOf="parent"
         app:layout_constraintEnd_toEndOf="parent"
         app:layout_constraintStart_toStartOf="parent"
         app:layout_constraintTop_toTopOf="parent"
         app:length="6"
         app:otp=""
         app:otp_text_size="20dp"
         app:width="40dp" />
```

****Original repo https://github.com/aabhasr1/OtpView****